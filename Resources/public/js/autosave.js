// === required options
//
// === unrequired otions
// url
// startCallback
// successCallback
// failCallback


$.fn.plDoAutosaveForm = function(options, changedElement) {
    var url = window.location.href;
    if(typeof options.url != "undefined"){
        url = options.url;
    }

    var startCallback = null;
    if(typeof options.startCallback != "undefined"){
        startCallback = options.startCallback;
    }

    var successCallback = null;
    if(typeof options.successCallback != "undefined"){
        successCallback = options.successCallback;
    }

    var failCallback = null;
    if(typeof options.failCallback != "undefined"){
        failCallback = options.failCallback;
    }

    var $formEl = $(this);
    var data = $formEl.serializeObject();
    if(startCallback){
        startCallback(changedElement);
    }
    $.ajax({
        url: url,
        data: data,
        //dataType: 'json',
        type: 'post',
        success: function(msg){
            if(successCallback){
                successCallback(msg, changedElement);
            }
        },
        error: function(msg){
            if(failCallback){
                failCallback(msg, changedElement);
            }
        }
    });
}


$.fn.plAutosaveForm = function (options) {
    if(typeof options == "undefined"){
        options = {};
    }

    var $formEl = $(this);


    $(function(){
        $formEl.find("input, select, textarea").change(function(){
            $formEl.plDoAutosaveForm(options, $(this));
        });
    });


}

