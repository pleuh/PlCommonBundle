
function refreshProgressBarOnEl(holderEl){
    if($(holderEl).has("data-progress")){
        var progress = $(holderEl).attr("data-progress");
        if($(holderEl).find(".plImageHolder-progress").length == 0){
            $(holderEl).append("<div class='plImageHolder-progress'><div class='plImageHolder-progress-bar-container'><div class='plImageHolder-progress-texte'></div><div class='plImageHolder-progress-bar'></div></div></div>");
        }

        var progressEl = $(holderEl).find(".plImageHolder-progress");
        $(progressEl).find(".plImageHolder-progress-bar").css("width", progress + "%");
        $(progressEl).find(".plImageHolder-progress-texte").html(parseFloat(progress).toFixed(0) + "%");
    }
    else if($(holderEl).find(".plImageHolder-progress").length){
        $(holderEl).find(".plImageHolder-progress").remove();
    }
}

function attachEventToElement(pickerDivEl){
    var loadingMethodExist = (typeof setLoading == "function"&& typeof finishLoading == "function");

    var width = $(pickerDivEl).attr("data-width");
    var height = $(pickerDivEl).attr("data-height");
    var data = {"width" : width, "height" : height};

    if(typeof $(pickerDivEl).attr("data-toFormat") != "undefined"){
        data["toFormat"] = $(pickerDivEl).attr("data-toFormat");
    }

    $(pickerDivEl).find('> div').click(function(){
        $(this).parent().trigger('click');
        return false;
    });


    if(!pickerDivEl.hasClass("dz-clickable")){
        $(pickerDivEl).dropzone({
            url: Routing.generate("pl_image2_download"),
            params: data,
            success: function(a, b){
                var result = typeof b === "string" ? JSON.parse(b) : b;
                if(result.hasOwnProperty("filepath") && result.hasOwnProperty("assetpath")){
                    // console.log(result.filepath);
                    $(pickerDivEl).css("z-index: 1; position: relative;");

                    var newElement = $('<img class="btnPickPlImage2" src="'+result.assetpath+'" data-width="'+width+'" data-height="'+height+'" style="max-width: 100%; position: absolute; top: 0; left: 0; right: 0; bottom: 0; z-index: 2; opacity: 0;" />');
                    $(pickerDivEl).closest('.plImageHolder').find('.plImageHolder-progress').remove();
                    $(pickerDivEl).closest('.plImageHolder').find('input').val(result.filepath);
                    $(pickerDivEl).closest('.plImageHolder').find('input').trigger('change');
                    $(pickerDivEl).after(newElement);
                    $(newElement).fadeTo(1000, 1, function(){
                        $(pickerDivEl).remove();
                        $(newElement).css("position", "static");
                        $(newElement).css("width", "auto");
                        $(newElement).css("height", "auto");
                        attachEventToElement(newElement);
                    });
                }
                else if(result.hasOwnProperty("error")){
                    // console.log(result.error);
                }
                else{
                    // console.log(b);
                }

                //var path = uploadFolderPrefix+"/"+b;
                //setImage(path, pickerDivEl);
                if(loadingMethodExist){
                    finishLoading($(pickerDivEl))
                }
            },
            uploadprogress: function(file, progress, bytesSent) {
                $(pickerDivEl).closest('.plImageHolder').attr("data-progress", progress);
                refreshProgressBarOnEl($(pickerDivEl).closest('.plImageHolder'));
            },
            error: function(a, b){
                if(loadingMethodExist){
                    finishLoading($(pickerDivEl))
                }
            },
            accept: function(file, done) {
                // console.log(file.size);
                if(file.size > 10*1024*1024){
                    alert("Le fichier que vous essayez d'envoyer doit faire moins de 10mo");
                    return;
                }
                else if ((/\.(jpg|jpeg|png|gif)$/i).test(file.name)){
                    if(loadingMethodExist){
                        setLoading($(pickerDivEl))
                    }
                    done();
                }
                else {
                    alert("Le fichier doit être au format jpg ou png");
                    return false;
                }
            },
            clickable: true
        });
    }

}

$(document).ready(function(){


    $('.btnPickPlImage2').initialize(function(){
        attachEventToElement($(this));
    });
    $('.btnPickPlImage2').each(function(){
        attachEventToElement($(this));
    });
    $('div.btnPickPlImage2').each(function(){
        var width = parseInt($(this).width());
        var dataWidth = parseInt($(this).attr("data-width"));
        var dataHeight = parseInt($(this).attr("data-height"));
        var newHeight = dataHeight * width / dataWidth;
        if(newHeight < parseInt($(this).height())){
            $(this).height(dataHeight * width / dataWidth);
        }
    });

    $('.btnRemove').click(function(){
        $(this).siblings("input").val("");
        $(this).siblings("img").fadeTo(200, 0.1);
    });
});