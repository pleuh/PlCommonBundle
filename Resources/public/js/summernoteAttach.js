// === required options
//
// === unrequired otions
// repository
// objectId
// toolbar
// styleTags
// minHeight

$.fn.plSummernote = function (options) {

    function refreshTextArea(el){
        $(el).parent().find('textarea.hiddenTxt').html($(el).summernote("code"))
    }

    $(this).each(function(){
        var $summernoteEl = $(this);

        var repository;
        var objectId;
        var toolbar = [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            // ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['codeview']]
        ];
        var styleTags = ['h1', 'h2', 'h3', 'p', 'blockquote'];

        if(typeof options == "undefined"){
           options = {}
        }

        if(typeof options.repository != "undefined"){
            repository = options.repository;
        }
        if(typeof options.objectId != "undefined"){
            objectId = options.objectId;
        }
        if(typeof options.toolbar != "undefined"){
            toolbar = options.toolbar;
        }
        if(typeof options.styleTags != "undefined"){
            styleTags = options.styleTags;
        }


        var minHeight = 350;
        if(typeof options.minHeight != "undefined"){
            minHeight = options.minHeight;
        }
        var height = 650;
        if(typeof options.height != "undefined"){
            height = options.height;
        }

        var colors = [
            ['#000000', '#424242', '#636363', '#9C9C94', '#CEC6CE', '#EFEFEF', '#F7F7F7', '#FFFFFF'],
            ['#FF0000', '#FF9C00', '#FFFF00', '#00FF00', '#00FFFF', '#0000FF', '#9C00FF', '#FF00FF'],
            ['#F7C6CE', '#FFE7CE', '#FFEFC6', '#D6EFD6', '#CEDEE7', '#CEE7F7', '#D6D6E7', '#E7D6DE'],
            ['#E79C9C', '#FFC69C', '#FFE79C', '#B5D6A5', '#A5C6CE', '#9CC6EF', '#B5A5D6', '#D6A5BD'],
            ['#E76363', '#F7AD6B', '#FFD663', '#94BD7B', '#73A5AD', '#6BADDE', '#8C7BC6', '#C67BA5'],
            ['#CE0000', '#E79439', '#EFC631', '#6BA54A', '#4A7B8C', '#3984C6', '#634AA5', '#A54A7B'],
            ['#9C0000', '#B56308', '#BD9400', '#397B21', '#104A5A', '#085294', '#311873', '#731842'],
            ['#630000', '#7B3900', '#846300', '#295218', '#083139', '#003163', '#21104A', '#4A1031']
        ];
        if(typeof options.colors != "undefined"){
            colors = options.colors;
        }

        var fontSizes = ['8', '9', '10', '11', '12', '14', '18', '24', '36'];
        if(typeof options.fontSizes != "undefined"){
            fontSizes = options.fontSizes;
        }

        var fontNames = [
            'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New',
            'Helvetica Neue', 'Helvetica', 'Impact', 'Lucida Grande',
            'Tahoma', 'Times New Roman', 'Verdana'
        ];
        if(typeof options.fontNames != "undefined"){
            fontNames = options.fontNames;
        }



        var onImageUploadCallback = null;
        if(typeof objectId != "undefined" && typeof repository != "undefined"){
            onImageUploadCallback = function (files) {
                var el = $(this);
                data = {"id": objectId, "type": "custom", "repository": repository};
                var formData = new FormData();
                var file = files[0]
                formData.append('file', file, file.name);
                $.ajax({
                    url: Routing.generate("adminUploadImage", data),
                    data: formData,
                    type: "post",
                    contentType: false,
                    processData: false,
                    success: function (path) {
                        var imgNode = document.createElement('img');
                        imgNode.setAttribute("src", path);
                        $(el).summernote('insertNode', imgNode);
                    },
                    error: function (msg) {
                    }
                });
            };
        }


        $summernoteEl.on('summernote.init', function () {
            // refreshTextArea($(this));
        }).on("summernote.blur", function () {
            refreshTextArea($(this));
        }).on("summernote.keyup", function () {
            refreshTextArea($(this));
        }).summernote({
            fontSize: 16,
            lang: 'fr-FR',
            minHeight: minHeight,
            height: 50,
            colors: colors,
            fontSizes: fontSizes,
            toolbar: toolbar,
            styleTags: styleTags,
            callbacks: {
                onImageUpload: onImageUploadCallback,
                onBlur: function(editable) {
                    var editor = editable.currentTarget.closest('.note-editor');
                    $(editor).siblings("textarea").trigger("change");
                },

                onKeyup: function (e) {
                    var el = $(this).closest('.fieldsetInput').find("textarea.hiddenTxt");
                    $(el).val($(this).summernote('code'));
                    $(el).change(); //To update any action binded on the control
                },
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        });
    })

    $(function(){
        $('.note-codable').keyup(function () {
            var el = $(this).closest(".note-editor").siblings("textarea.hiddenTxt");
            $(el).val($(this).val());
        });
    });
}