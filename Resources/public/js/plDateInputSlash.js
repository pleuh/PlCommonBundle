// === unrequired otions
// typeInput = standard|month

$.fn.plDateInputSlash = function(options) {
    var typeInput = "standard"
    if(typeof options != "undefined" && typeof options.typeInput != "undefined"){
        if(["standard","month"].indexOf(options.typeInput) === -1){
            console.error("in plDateInputSlash.js, l'option 'typeInput' doit valoir standard ou month");
            return;
        }
        typeInput = options.typeInput;
    }

    var $element = $(this);
    if(typeInput == "month"){
        $element.attr("maxlength", 7);
    }
    else{
        $element.attr("maxlength", 10);
    }


    $element.bind('keyup', 'keydown', function (e) {
        //To accomdate for backspacing, we detect which key was pressed - if backspace, do nothing:
        if(e.which !== 8) {
            var numChars = $element.val().length;
            if(numChars > 10 || (numChars > 6 && typeInput == "month")){
                return false;
            }
            if (numChars === 2 || (numChars === 5 && typeInput != "month")){
                var thisVal = $element.val();
                thisVal += '/';
                $element.val(thisVal);
            }
            if(e.which == 111 || e.which == 191){
                var thisVal = $element.val();
                thisVal = thisVal.replace(/\/\//g, "/");
                $element.val(thisVal);
            }
        }
    });
}