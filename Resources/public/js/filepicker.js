function upload(services, width, height, fieldId, path, fit){
	var services = services;
	var width = width;
	var height = height;
	var fieldId = fieldId;
	var path = path;
	var fit = fit;
	var imageId = fieldId + '_picker';
	var oldFile = $('#'+fieldId).val();


	var dataSet = {format: 'jpg', fit: fit};
	if(!isNaN(height)){
		dataSet["height"] = height;
	}
	if(!isNaN(width)){
		dataSet["width"] = width;
	}

	filepicker.pick({
		mimetypes: ['image/*', 'text/plain'],
		container: 'window',
		services:services,
	},
	function(FPFile){
		if($('#filepickLoading').length > 0){
			$('#filepickLoading').css("opacity", 1);
		}
		filepicker.convert(FPFile, dataSet,
			function(file){
				$.ajax({
					url: urlPicker,
					data: {file: file, oldFile: oldFile, path: path},
					type: 'post',
					dataType: 'json',
					success: function(newUrl){
							// console.log(newUrl);
							$('#'+fieldId).val(newUrl);
							$('#'+imageId).attr("src", basePathUrl + newUrl);
							 $('#filepickLoading').css("opacity", 0);
					},
					error: function(msg){
						 // console.log(msg);
						 console.log("endload");
						 $('#filepickLoading').css("opacity", 0);
						}
					})
			});
	});
}

$(document).ready(function(){

	$('.btnFilepicker').click(function(){
		var element = $(this);;
		upload($.parseJSON($(element).attr('data-services')), parseInt($(element).attr('data-width')), parseInt($(element).attr('data-height')), $(element).attr("id").substring(0, $(element).attr("id").length - 7), $(element).attr('data-path'), $(element).attr('data-fit'));
	});

	$('.uploadBtn').click(function(){
		var element = $(this).parent().children("img");
		upload($.parseJSON($(element).attr('data-services')), parseInt($(element).attr('data-width')), parseInt($(element).attr('data-height')), $(element).attr("id").substring(0, $(element).attr("id").length - 7), $(element).attr('data-path'), $(element).attr('data-fit'));
	});
});