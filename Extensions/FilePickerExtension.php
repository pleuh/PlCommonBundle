<?php
# Test/MyBundle/DependencyInjection/MyBundleExtension.php

namespace Pl\CommonBundle\Extensions;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FilePickerExtension extends \Twig_Extension
{
	private $apiKey, $container;
	public function __construct(ContainerInterface $container){
		$this->container = $container;
		$this->apiKey = $this->container->getParameter("pl_common.filepicker_apikey");
	}
	
    public function getFunctions()
    {
	    return array(
	        new \Twig_SimpleFunction('pl_filepicker_javascript', array($this, 'pl_filepicker_javascript'), array('is_safe' => array('html'))),
	        new \Twig_SimpleFunction('pl_filepicker_stylesheet', array($this, 'pl_filepicker_stylesheet'), array('is_safe' => array('html'))),
        );
    }
	
    public function getFilters()
    {
        return array(
         
        );
    }


	public function pl_filepicker_javascript(){
		$str = '';

		$str .= '
		<!-- load file picker api -->
		<script type="text/javascript">
		(function(a){if(window.filepicker){return}var b=a.createElement("script");b.type="text/javascript";b.async=!0;b.src=("https:"===a.location.protocol?"https:":"http:")+"//api.filepicker.io/v1/filepicker.js";var c=a.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);var d={};d._queue=[];var e="pick,pickMultiple,read,write,writeUrl,export,convert,store,storeUrl,remove,stat,setKey,constructWidget,makeDropPane".split(",");var f=function(a,b){return function(){b.push([a,arguments])}};for(var g=0;g<e.length;g++){d[e[g]]=f(e[g],d._queue)}window.filepicker=d})(document);
		</script>
		<script type="text/javascript">
		filepicker.setKey("'.$this->apiKey.'");
		</script>';

		$urlPicker = $this->container->get('router')->generate('pl_filePicker');
		$str .= "<script type='text/javascript'>
		var urlPicker = '" . $urlPicker ."'
		var basePathUrl = '" . $this->container->get('request')->getBasePath().'/' ."'
		</script>";

		$str .= '<script type="text/javascript" src="'.$this->container->get('templating.helper.assets')->getUrl('bundles/plcommon/js/filepicker.js').'"></script>';
		echo($str);
	}

	public function pl_filepicker_stylesheet(){
		$str = '';
		$str .= '<link rel="stylesheet" type="text/css" href="'.$this->container->get('templating.helper.assets')->getUrl('bundles/plcommon/css/filepicker.css').'"></script>';
		echo($str);
	}


	
    public function getName()
    {
        return 'pl.extension.filepicker';
    }

}

















