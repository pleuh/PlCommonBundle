<?php
# Test/MyBundle/DependencyInjection/MyBundleExtension.php

namespace Pl\CommonBundle\Extensions;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PlImage2Extension
 * @package Pl\CommonBundle\Extensions
 * @property Packages $assets
 * @property RouterInterface $router
 */
class PlImage2Extension extends \Twig_Extension
{
	private $assets, $router;

	/**
	 * PlImage2Extension constructor.
	 * @param Packages $assets
	 * @param RouterInterface $router
	 */
	public function __construct(Packages $assets, RouterInterface $router){
		$this->assets = $assets;
		$this->router = $router;
	}
	
 

	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('plImageExists', [$this, 'plImageExists']),
			new \Twig_SimpleFunction('pl_image2_js', [$this, 'pl_image2_js'], ['is_safe' => ['html']]),
			new \Twig_SimpleFunction('pl_image2_css', [$this, 'pl_image2_css'], ['is_safe' => ['html']]),
		];
	}


	public function plImageExists($imagePath){
		return file_exists($imagePath);
	}

	public function pl_image2_js(){


		$str = '';

		$str .= '<script type="text/javascript" src="'.$this->assets->getUrl('bundles/plcommon/js/dropzone.js').'?v=1.00"></script>';
		$str .= '<script type="text/javascript" src="'.$this->assets->getUrl('bundles/plcommon/js/initialize.js').'?v=1.00"></script>';
		$str .= '<script type="text/javascript" src="'.$this->assets->getUrl('bundles/plcommon/js/plimage2.js').'?v=1.00"></script>';
		$str .= '<script type="text/javascript" src="'.$this->assets->getUrl('bundles/fosjsrouting/js/router.js').'?v=1.00"></script>';
		$str .= '<script type="text/javascript" src="'.$this->router->generate("fos_js_routing_js", ["callback" => "fos.Router.setData"]).'"></script>';
		echo($str);
	}

	public function pl_image2_css(){
		$str = '';
		$str .= '<link rel="stylesheet" type="text/css" href="'.$this->assets->getUrl('bundles/plcommon/css/plimage2.css').'"></script>';
		echo($str);
	}


	
    public function getName()
    {
        return 'pl.extension.image2';
    }

}

















