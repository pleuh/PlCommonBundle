<?php
# Test/MyBundle/DependencyInjection/MyBundleExtension.php

namespace Pl\CommonBundle\Extensions;
use Symfony\Component\Asset\Packages;

/**
 * Class PlSummernoteExtension
 * @package Pl\CommonBundle\Extensions
 * @property Packages $assets
 */
class PlSummernoteExtension extends \Twig_Extension
{
	private $assets;
	public function __construct(Packages $assets){
		$this->assets = $assets;
	}
	
 

	public function getFunctions()
	{
		return array(
			new \Twig_SimpleFunction('pl_summernote_js', array($this, 'pl_summernote_js'), array('is_safe' => array('html'))),
			new \Twig_SimpleFunction('pl_summernote_css', array($this, 'pl_summernote_css'), array('is_safe' => array('html'))),
		);
	}


	public function plImageExists($imagePath){
		return file_exists($imagePath);
	}

	public function pl_summernote_js(){
		$str = '';
		$str .= '<script type="text/javascript" src="'.$this->assets->getUrl('bundles/plcommon/js/summernote.js').'"></script>';
		$str .= '<script type="text/javascript" src="'.$this->assets->getUrl('bundles/plcommon/js/summernote-fr-FR.js').'"></script>';
		echo($str);
	}

	public function pl_summernote_css(){
		$str = '';
		$str .= '<link rel="stylesheet" type="text/css" href="'.$this->assets->getUrl('bundles/plcommon/css/summernote.css').'"></script>';
		echo($str);
	}


	
    public function getName()
    {
        return 'pl.extension.summernote';
    }

}

















