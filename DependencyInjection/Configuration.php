<?php

namespace Pl\CommonBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('pl_common');

        $rootNode
            ->children()
				->scalarNode('filepicker_apikey')->defaultValue('ABSoK12JxTLmmIxqbHuTXz')->end()
				->scalarNode('max_login_attemps')->defaultValue(null)->end()
				->scalarNode('sitemap_provider')->defaultValue(null)->end()
				->integerNode('waiting_time_secs')->defaultValue(5*60)->end()
				->scalarNode('prevent_indexation')->defaultValue(false)->end()
				->scalarNode('geocode_listener')->defaultValue(true)->end()
				->scalarNode('maxIdleTime')->defaultValue(null)->end()
				->scalarNode('upload_tmp_dir')->defaultValue('uploads/tmp/')->end()
				->scalarNode('node_path')->defaultValue('/usr/bin/node')->end()
				->scalarNode('node_modules_path')->defaultValue('/usr/lib/node_modules')->end()
				->scalarNode('instagram_token')->defaultValue(null)->end()
            ->end();

        return $treeBuilder;
    }
}
