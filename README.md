PlCommonBundle V2
======

Ensemble de fonction, methodes, services Symfony


Dépendances
======
Ce package dépend de :
- bazinga geocoder (https://github.com/geocoder-php/BazingaGeocoderBundle)
- liip imagine (https://github.com/liip/LiipImagineBundle)