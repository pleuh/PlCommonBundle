<?php

namespace Pl\CommonBundle\Listener;

use Geocoder\Exception\NoResult;
use Geocoder\Exception\QuotaExceeded;
use Pl\CommonBundle\Interfaces\GeocodableInterface;
use Pl\CommonBundle\Manager\LocalisationManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class GeocodeListener
 * @package Pl\CommonBundle\Listener
 * @property ContainerInterface $container
 * @property bool $geocodeListener
 */
class GeocodeListener
{

	private $container;
	private $geocodeListener;

	/**
	 * GeocodeListener constructor.
	 * @param LocalisationManager $container
	 * @param bool $geocodeListener
	 */
	public function __construct(ContainerInterface $container, $geocodeListener){
		$this->container = $container;
		$this->geocodeListener = $geocodeListener;
	}


	public function prePersist(\Doctrine\ORM\Event\LifecycleEventArgs $args){
		if(!$this->geocodeListener){
			return;
		}
		$entity = $args->getEntity();
		if(in_array(GeocodableInterface::class, class_implements($entity)) && !$entity->getHasBeenGeocoded()){
			try{
				$this->container->get(LocalisationManager::class)->geocode($entity, false);
			}
			catch(QuotaExceeded | \Geocoder\Exception\Exception | \Exception $e){
			}
		}

	}

	public function preUpdate(\Doctrine\ORM\Event\PreUpdateEventArgs $args){
		if(!$this->geocodeListener){
			return;
		}

		$entity = $args->getEntity();
		if(in_array(GeocodableInterface::class, class_implements($entity))){

		}
		if(in_array(GeocodableInterface::class, class_implements($entity))){
			if(!$entity->getHasBeenGeocoded() || count(array_intersect(["nom", "ville", "codePostal"], array_keys($args->getEntityChangeSet())))){
				try{
					$this->container->get(LocalisationManager::class)->geocode($entity, false);
				}
				catch(QuotaExceeded | \Geocoder\Exception\Exception | \Exception $e){
				}
			}
		}
	}
}
