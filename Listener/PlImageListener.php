<?php

namespace Pl\CommonBundle\Listener;


use League\Flysystem\AdapterInterface;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class PlImageListener
 * @package App\Listener
 * @property string $tmpDir
 * @property FilesystemInterface $filesystem
 */
class PlImageListener
{

	private $tmpDir;
	private $filesystem;

	public function __construct($tmpDir, FilesystemInterface $filesystem){
		$this->tmpDir = $tmpDir;
		$this->filesystem = $filesystem;
	}


	public function preUpdate(\Doctrine\ORM\Event\PreUpdateEventArgs $args){
		$entity = $args->getEntity();

		foreach(get_class_methods($entity) as $method){
			if(preg_match('#get(.+)2ImagePath#', $method, $m)) {
				$property = substr(strtolower($m[1]), 0, 1).substr($m[1], 1);

				if(array_key_exists($property, $args->getEntityChangeSet())){
					if(preg_match('#'.$this->tmpDir.'#', $args->getEntityChangeSet()[$property][1])){
						//Si une image arrive dans les TMP remove old image
						$supposedDestPath = $args->getEntityChangeSet()[$property][0];
						if($supposedDestPath && $this->filesystem->has($supposedDestPath)){
							$this->filesystem->delete($supposedDestPath);
						}
					}

					//On passe d'une valeur non vide à une valeur vide
					if($args->getEntityChangeSet()[$property][0] && !$args->getEntityChangeSet()[$property][1]){
						if($this->filesystem->has("public/".$args->getEntityChangeSet()[$property][0])){
							$this->filesystem->delete("public/".$args->getEntityChangeSet()[$property][0]);
						}
					}
				}
			}
		}
		$this->correctAndSetImageToFolder($args->getEntity());
	}

	public function prePersist(\Doctrine\ORM\Event\LifecycleEventArgs $args){
		$this->correctAndSetImageToFolder($args->getEntity());
	}


	public function preRemove(\Doctrine\ORM\Event\LifecycleEventArgs $args){
		$entity = $args->getEntity();
		foreach(get_class_methods($entity) as $method){
			if(preg_match('#get(.+)2ImagePath#', $method, $m)){
				$property = substr(strtolower($m[1]), 0, 1).substr($m[1], 1);
				$getMethod = "get".ucfirst($property);
				$filePath = $entity->$getMethod();
				if($filePath && $this->filesystem->has($filePath)){
					$this->filesystem->delete($filePath);
				}
			}
		}
	}

	private function correctAndSetImageToFolder($entity){
		foreach(get_class_methods($entity) as $method){
			if(preg_match('#get(.+)2ImagePath#', $method, $m)){
				$property = substr(strtolower($m[1]), 0, 1).substr($m[1], 1);
				$getMethod = "get".ucfirst($property);
				$setMethod = "set".ucfirst($property);

				if($entity->$getMethod() != null){
					if(!file_exists($entity->$getMethod())){
						continue;
					}

					$tmpFile = new File($entity->$getMethod());
					if(preg_match('#'.$this->tmpDir.'#', $tmpFile->getRealPath())){
						$newPath = $entity->$method().'.'.preg_replace('#jpeg#', 'jpg', $tmpFile->guessExtension());
						$this->filesystem->put("public/" . $newPath, file_get_contents($tmpFile), [ 'visibility' => AdapterInterface::VISIBILITY_PUBLIC]);
						$entity->$setMethod($newPath);
					}
				}
			}
		}
	}
}
