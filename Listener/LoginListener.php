<?php

namespace Pl\CommonBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Pl\CommonBundle\Manager\LocalisationManager;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\DependencyInjection\Container;


/**
 * Class LoginListener
 * @package Pl\CommonBundle\Listener
 * @property Container $container
 */
class LoginListener extends \Twig_Extension
{

	const PL_LOGIN_SUCCESS_EVENT = "pl.event.loginsuccess";
	private $container;
	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('isLoginBlocked', [$this, 'isLoginBlocked']),
		];
	}
	
	private function getIpKey(){
		return "loginFailures_" .preg_replace("#[^0-9]#", "", $this->container->get('request_stack')->getMasterRequest()->getClientIp());
	}

	private function getCurrentLoginFailsCount(){
		$cache = new FilesystemAdapter("", $this->container->getParameter("pl_common.waiting_time_secs"));
		$loginFailuresCache = $cache->getItem($this->getIpKey());

		if(!$loginFailuresCache->isHit()){
			$loginFailures = 0;
		}
		else{
			$loginFailures = $loginFailuresCache->get();
		}
		return $loginFailures;
	}

	public function isLoginBlocked(){

		if($this->container->getParameter("pl_common.max_login_attemps") != null) {
			return $this->getCurrentLoginFailsCount() >= $this->container->getParameter("pl_common.max_login_attemps");
		}
		return false;
	}

	public function onLoginFailedEvent()
	{
		if($this->container->getParameter("pl_common.max_login_attemps") != null){
			$failsCont = $this->getCurrentLoginFailsCount();
			$failsCont += 1;
			$cache = new FilesystemAdapter("", $this->container->getParameter("pl_common.waiting_time_secs"));
			$loginFailuresCache = $cache->getItem($this->getIpKey());
			$loginFailuresCache->set($failsCont);
			$cache->save($loginFailuresCache);

		}
	}

	public function onLoginSuccessEvent()
	{
		$this->resetCount();
	}
	private function resetCount() {
		$cache = $this->container->get("liip_doctrine_cache.ns.mynamespace");
		$cache->delete($this->getIpKey());
	}



	public function getName()
	{
		return "pl.listener.login";
	}
}
