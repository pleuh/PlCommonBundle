<?php
namespace Pl\CommonBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Role\SwitchUserRole;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class SwitchUserListener
 * @package Pl\CommonBundle\Listener
 * @property Container $container
 */
class SwitchUserListener
{

	private $container;
	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function onKernelResponse(FilterResponseEvent $event)
	{
		if($this->container->get('request_stack')->getMasterRequest()->get('_route') != "fos_user_security_logout"){
			$authChecker = $this->container->get('security.authorization_checker');
			$impersonatingUser = null;
			try{
				if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
					foreach ($this->container->get("security.token_storage")->getToken()->getRoles() as $role) {
						if ($role instanceof SwitchUserRole) {
							$impersonatingUser = $role->getSource()->getUser();
							break;
						}
					}
				}
				if($impersonatingUser){
					$response = $event->getResponse();
					$htmlContent = $response->getContent();

					$user = $this->container->get("security.token_storage")->getToken()->getUser();
					$allSwitchUserBar = '<div id="switchUser"><div><i class="fa fa-fw fa-warning"></i>
						'.$this->container->get('translator')->trans('switchUser.connecte', ['%username%' => $user->getUsername()]).'  <i class="fa fa-fw fa-warning"></i></div>
						<a class="btn btn-small btn-transparent btn-white" href="'.$this->container->get('router')->generate('sonata_admin_dashboard').'?_switch_user=_exit" class="">'.$this->container->get('translator')->trans('switchUser.btnRevenir').'</a>
					</div>';
					$htmlContent = preg_replace('#<div id="flashContainer"#', sprintf('%s<div id="flashContainer"', $allSwitchUserBar), $htmlContent);
					$response->setContent($htmlContent);
				}
			}
			catch(\Exception $e){

			}
		}
	}
}
