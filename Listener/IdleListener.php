<?php

namespace Pl\CommonBundle\Listener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * Class IdleListener
 * @package Pl\CommonBundle\Listener
 * @property Container $container
 */
class IdleListener extends \Twig_Extension
{

	protected $session;
	protected $tokenStorage;
	protected $router;
	protected $maxIdleTime;

	public function __construct(
		SessionInterface $session,
		TokenStorageInterface $tokenStorage,
		RouterInterface $router,
		$maxIdleTime = null
	){
		$this->session = $session;
		$this->tokenStorage = $tokenStorage;
		$this->router = $router;
		$this->maxIdleTime = $maxIdleTime;
	}

	public function onKernelRequest(GetResponseEvent $event){
		if($this->maxIdleTime){
			if(HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()){
				return;
			}

			if($this->maxIdleTime > 0){
				$this->session->start();
				$lapse = time() - $this->session->getMetadataBag()->getLastUsed();
				if($lapse > $this->maxIdleTime){
					$this->tokenStorage->setToken(null);
					$this->session->invalidate();
					$this->session->getFlashBag()->set('success', ["message" => 'Vous avez été deconnecté après une période d\'inactivité']);

					$event->setResponse(new RedirectResponse($this->router->generate('logout')));
				}
			}
		}
	}
}
