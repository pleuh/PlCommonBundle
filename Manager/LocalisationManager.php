<?php

namespace Pl\CommonBundle\Manager;

use App\Interfaces\LoggableInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Geocoder\Exception\InvalidServerResponse;
use Geocoder\Model\Address;
use Geocoder\Model\AddressCollection;
use Geocoder\Provider\GoogleMaps\Model\GoogleAddress;
use Geocoder\Provider\Provider;
use Geocoder\Query\GeocodeQuery;
use Pl\CommonBundle\Interfaces\GeocodableInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Simple\FilesystemCache;

/**
 * Class LocalisationManager
 * @package Pl\CommonBundle\Manager
 * @property EntityManager $em
 * @property Provider $geocoder
 * @property LoggerInterface $logger
 */
class LocalisationManager
{
	private $geocoder, $em, $logger;

	public function __construct(EntityManagerInterface $em, Provider $geocoder, LoggerInterface $logger){
		$this->em = $em;
		$this->geocoder = $geocoder;
		$this->logger = $logger;
	}


	/**
	 * @param $query
	 * @return null|PlLocalisationPoint
	 */
	public function getLatLngFromQuery($query){
		if(!$query || strlen($query) < 2){
			return null;
		}
		$cache = new FilesystemCache();
		$cacheKey = "GeocodeQuery_______" . preg_replace('#[^a-zA-Z0-9]#', '', $query);

		if(!$cache->has($cacheKey)){
			$resultToPutInCache = null;
			try{
				$result = $this->geocodeFromString($query);
				if($result instanceof AddressCollection && count($result) > 0){
					/** @var Address $address */
					$address = $result->get(0);
					if(is_subclass_of($address, Address::class)){
						/** @var GoogleAddress $address */
						if($address->getCoordinates()->getLatitude() && $address->getCoordinates()->getLongitude()){
							$resultToPutInCache = new PlLocalisationPoint($address->getCoordinates()->getLatitude(), $address->getCoordinates()->getLongitude());
						}
					}
				}
			}
			catch(\Geocoder\Exception\Exception $e){
				$this->logger->error($e->getMessage());
				$resultToPutInCache = null;
			}
			$cache->set($cacheKey, $resultToPutInCache);

			return $resultToPutInCache;
		}

		return $cache->get($cacheKey);
	}

	public function distanceMeters(GeocodableInterface $pos1, GeocodableInterface $pos2){
		return $this->distanceMetersBetweenLatLng($pos1->getLat(), $pos1->getLng(), $pos2->getLat(), $pos2->getLng());
	}

	public function distanceMetersBetweenLatLng($lat1, $lng1, $lat2, $lng2){
		$earthRadius = 3958.75;

		$dLat = deg2rad($lat2 - $lat1);
		$dLng = deg2rad($lng2 - $lng1);


		$a = sin($dLat / 2) * sin($dLat / 2) +
			cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLng / 2) * sin($dLng / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$dist = $earthRadius * $c;

		// from miles
		$meterConversion = 1609;
		$geopointDistance = $dist * $meterConversion;

		return $geopointDistance;
	}

	/**
	 * @param GeocodableInterface $geocodable
	 * @param bool $bPersist
	 * @throws InvalidServerResponse
	 */
	public function geocode(GeocodableInterface $geocodable, $bPersist = true){
		try{
			$strGeocodable = (string)$geocodable;
			if(preg_match('#\d{5}#', $strGeocodable)){
				$strGeocodable .= ' FRANCE';
			}

			$result = $this->geocodeFromString($strGeocodable);
		}
		catch(\Geocoder\Exception\Exception $e){
			$geocodable->setHasBeenGeocoded(true);
			if($bPersist){
				$this->em->persist($geocodable);
				$this->em->flush();
			}
			throw new InvalidServerResponse($e->getMessage());
		}

		if(get_class($result) == AddressCollection::class && count($result) > 0){
			/** @var Address $result */
			$result = $result->get(0);
		}

		/** @var Address $result */
		if(is_subclass_of($result, Address::class)){

			if(method_exists($geocodable, "getVille") && $geocodable->getVille() == null && $result->getLocality() != null){
				$geocodable->setVille($result->getLocality());
			}

			$codePostal = $result->getPostalCode() ? $result->getPostalCode() : null;
			if(method_exists($geocodable, "getCodePostal") && $geocodable->getCodePostal() == null && $codePostal){
				$geocodable->setCodePostal($codePostal);
			}


			if(method_exists($geocodable, "setCodeDepartement") && $geocodable->getCodeDepartement() == null && $codePostal != null){
				$geocodable->setCodeDepartement(substr($codePostal, 0, 2));
			}


			if(count($result->getAdminLevels()) > 0 && method_exists($geocodable, "setNomRegion")){
				$nomRegion = $result->getAdminLevels()->get(1)->getName();
				$geocodable->setNomRegion($nomRegion);

				if(method_exists($geocodable, "setCodeRegion")){
					$regionByName = [
						"Auvergne-Rhône-Alpes" =>  84,
						"Bourgogne-France-Comté" =>  27,
						"Bretagne" =>  53,
						"Centre-Val de Loire" =>  24,
						"Corse" =>  94,
						"Grand Est" =>  44,
						"Hauts-de-France" =>  32,
						"Île-de-France" =>  11,
						"Normandie" =>  28,
						"Nouvelle-Aquitaine" =>  75,
						"Occitanie" =>  76,
						"Pays-de-la-Loire" =>  52,
						"Provence-Alpes-Côte-d'Azur" =>  93,
						"Dom-Tom" =>  100,
					];
					if(array_key_exists($nomRegion, $regionByName)){
						$geocodable->setCodeRegion($regionByName[$nomRegion]);
					}
				}
			}
			if(count($result->getAdminLevels()) > 1 && method_exists($geocodable, "setNomDepartement") && $geocodable->getNomDepartement() == null){
				$geocodable->setNomDepartement($result->getAdminLevels()->get(2)->getName());
			}



			if($result->getCoordinates()->getLatitude() != null){
				$geocodable->setLat($result->getCoordinates()->getLatitude());
			}
			if($result->getCoordinates()->getLongitude()){
				$geocodable->setLng($result->getCoordinates()->getLongitude());
			}
		}

		$geocodable->setHasBeenGeocoded(true);

		if($bPersist){
			$this->em->persist($geocodable);
			$this->em->flush();
		}
	}

	public function geocodeFromString($string){
		return $this->geocoder->geocodeQuery(GeocodeQuery::create($string));
	}


}

class PlLocalisationPoint
{
	private $lat, $lng;

	/**
	 * PlLocalisationPoint constructor.
	 * @param $lat
	 * @param $lng
	 */
	public function __construct($lat, $lng){
		$this->lat = $lat;
		$this->lng = $lng;
	}

	/**
	 * @return mixed
	 */
	public function getLat(){
		return $this->lat;
	}

	/**
	 * @return mixed
	 */
	public function getLng(){
		return $this->lng;
	}
}