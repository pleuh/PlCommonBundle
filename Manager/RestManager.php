<?php

namespace Pl\CommonBundle\Manager;
use Doctrine\ORM\EntityManager;
use Bazinga\Bundle\GeocoderBundle\Geocoder\LoggableGeocoder;
use Pl\CommonBundle\Interfaces\GeocodableInterface;
use Symfony\Component\HttpFoundation\Request;

class RestManager{
	private $validator, $returnType, $authType, $user, $password, $token;
	public function __construct($validator){
		$this->validator = $validator;
		$this->returnType = self::RETURN_TYPE_JSON;
		$this->authType = self::AUTH_TOKEN;
		$this->user = null;
		$this->password = null;
		$this->token = null;
	}


	const RETURN_TYPE_JSON = "RETURN_TYPE_JSON";
	const RETURN_TYPE_PLAIN = "RETURN_TYPE_PLAIN";
	public function setReturnType($returnType){
		$this->returnType = $returnType;
	}

	const AUTH_USERPASS = "AUTH_USERPASS";
	const AUTH_TOKEN_BEARER = "AUTH_TOKEN_BEARER";
	const AUTH_TOKEN = "AUTH_TOKEN";
	public function setAuthType($authType){
		$this->authType = $authType;
	}

	/**
	 * @param string $token
	 */
	public function setToken($token){
		$this->token = $token;
	}
	/**
	 * @param string $user
	 */
	public function setUser($user){
		$this->user = $user;
	}
	/**
	 * @param string $password
	 */
	public function setPassword($password){
		$this->password = $password;
	}


	/**
	 * @param $url
	 * @param string $method
	 * @param null $parameters
	 * @param bool $json
	 * @return mixed
	 * @throws RestConnexionException
	 */
	public function request($url, $method = Request::METHOD_POST, $parameters = null, $json = true){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [];

		switch($this->authType){
			case self::AUTH_USERPASS:{
				if($this->user != null && $this->password != null){
					curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->password);
				}
				break;
			}
			case self::AUTH_TOKEN_BEARER:{
				if($this->token){
					array_push($headers, 'Authorization: Bearer ' . $this->token);
				}
				break;
			}
			case self::AUTH_TOKEN:
			default:{
				if($this->token){
					$query = parse_url($url, PHP_URL_QUERY);
					if ($query) {
						$url .= '&access_token='.$this->token;
					} else {
						$url .= '?access_token='.$this->token;
					}
				}
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				break;
			}
		}

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		if($method == "POST"){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		}
		elseif($method == "PUT"){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		}

		if($parameters != null){
			if($json){
				$parameters = json_encode($parameters);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
				array_push($headers, 'Content-Type: application/json');
				array_push($headers, 'Content-Length: ' . strlen($parameters));
			}
			else{
				$strPost = "";
				foreach($parameters as $key => $value){
					$strPost .= "&".$key."=".$value;
				}
				$strPost = substr($strPost, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $strPost);
				array_push($headers, 'Content-Type: text/plain');
				array_push($headers, 'Content-Length: ' . strlen($strPost));
			}
		}
		if(count($headers) > 0){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

		$rep = curl_exec($ch);

		if($rep === false){
			throw new RestConnexionException(curl_error($ch));
		}

		if($this->returnType == self::RETURN_TYPE_JSON){
			$rep = json_decode($rep);
		}
		return $rep;
	}


	public function getJsonValidationError($entity, $validationGroups){
		$errors = $this->validator->validate($entity, $validationGroups);

		if(count($errors) > 0){
			$arRet = [];
			foreach ($errors as $error) {
				array_push($arRet, ["message" => $error->getMessage(), "property_path" => $error->getPropertyPath()]);
			}
			return ["error" => $arRet];
		}
		return null;
	}


}


class RestConnexionException extends \Exception{

}