<?php

namespace Pl\CommonBundle\Manager;

use Imagine\Image\ImagineInterface;
use Liip\ImagineBundle\Imagine\Filter\Loader\ThumbnailFilterLoader;
use Pl\CommonBundle\OtherClasses\Image;
use Symfony\Component\HttpFoundation\File\File;

class ImageManager
{
	protected $imagine;

	public function __construct(ImagineInterface $imagine){
		$this->imagine = $imagine;
	}

	/**
	 * @param $path
	 * @param null $width
	 * @param null $height
	 * @param null $toFormat
	 */
	public function redimensionerImage($path, $width = null, $height = null, $toFormat = null){
		if(!$width && !$height){
			throw new \RuntimeException("Il faut définir width ou height pour pouvoire redimenssioner l'image");
		}
		$image = $this->imagine->open($path);

		if($image->getSize()->getWidth() == $width && $image->getSize()->getHeight() == $height){
			return;
		}

		$image2 = new Image($image->getGdResource(), $image->palette(), $image->metadata());
		if($width != null && $height == null){
			if($width == $image2->getSize()->getWidth()){
				return; // dont apply filter if same size
			}
			$height = $width * $image2->getSize()->getHeight() / $image2->getSize()->getWidth();
		}
		elseif($height != null && $width == null){
			if($height == $image2->getSize()->getHeight()){
				return; // dont apply filter if same size
			}
			$width = $height * $image2->getSize()->getWidth() / $image2->getSize()->getHeight();
		}

		$thumbnailFilter = new ThumbnailFilterLoader();
		$newImage = $thumbnailFilter->load($image2, [
			"size" => [$width, $height],
			"allow_upscale" => true,
			"mode" => "THUMBNAIL_INSET",
		]);
		$newImage->save($path);
	}

	/**
	 * @param $path
	 * @param $toFormat
	 * @param bool $bAndDelete
	 * @return string
	 */
	public function convertToFormat($path, $toFormat, $bAndDelete = true){
		if(!in_array($toFormat, ["jpg", "png"])){
			throw new \RuntimeException(sprintf("%s is not supported by method convertToFormat", $toFormat));
		}
		$file = new File($path);

		if($file->getExtension() == $toFormat){
			return $path;
		}

		if(preg_match('#(.+)'.$file->getBasename().'#', $path, $m)){
			$toPath = $m[1] . $file->getBasename('.' . $file->getExtension()) . "." .$toFormat;
		}
		$this->imagine->open($path)->save($toPath);
		if($bAndDelete){
			unlink($path);
		}
		return $toPath;
	}


}