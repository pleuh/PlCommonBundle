<?php

namespace Pl\CommonBundle\Manager;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class InstagramManager
 * @package App\Manager
 * @property RestManager $restManager
 * @property string $token
 */
class InstagramManager
{

	protected $restManager;
	protected $token;

	/**
	 * UserManager constructor.
	 * @param RestManager $restManager
	 * @codeCoverageIgnore
	 */
	public function __construct(
		RestManager $restManager,
		?string $token
	){
		$this->restManager = $restManager;
		$this->token = $token;
	}

	private static function getCacheKey($nMax){
		return "INSTACACHE" . $nMax;
	}

	/**
	 * @return InstagramPost[]
	 */
	public function getLastPosts($nMax = 20){
		if(!$this->token){
			throw new \RuntimeException("You must provide an instagram token");
		}

		$cache = new FilesystemAdapter("", 3600);
		$cacheItem = $cache->getItem(self::getCacheKey($nMax));

		if(!$cacheItem->isHit()){
			$this->restManager->setToken($this->token);
			$arRet = [];
			try{
				$response = $this->restManager->request("https://api.instagram.com/v1/users/self/media/recent/?access_token", Request::METHOD_GET);
				if(property_exists($response, "data")){
					foreach(array_slice($response->data, 0, $nMax) as $data){
						$arRet[] = new InstagramPost(
							$data->images->thumbnail->url
							, $data->images->low_resolution->url
							, $data->images->standard_resolution->url
							, $data->caption->text
							, $data->likes->count
							, $data->link
						);
					}
				}
			}
			catch(RestConnexionException $e){}

			$cacheItem->set($arRet);
			$cache->save($cacheItem);
		}
		else{
			$arRet = $cacheItem->get();
		}


		return $arRet;
	}
}


class InstagramPost
{
	public $thumbnailUrl;
	public $lowResUrl;
	public $highResUrl;
	public $text;
	public $nLikes;
	public $link;


	/**
	 * InstagramPost constructor.
	 * @param $thumbnailUrl
	 * @param $lowResUrl
	 * @param $highResUrl
	 * @param $text
	 * @param $nLikes
	 * @param $link
	 */
	public function __construct($thumbnailUrl, $lowResUrl, $highResUrl, $text, $nLikes, $link){
		$this->thumbnailUrl = $thumbnailUrl;
		$this->lowResUrl = $lowResUrl;
		$this->highResUrl = $highResUrl;
		$this->text = $text;
		$this->nLikes = $nLikes;
		$this->link = $link;
	}
}
