<?php

namespace Pl\CommonBundle\Manager;

use App\Exception\GenerateCssException;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class NodeManager
 * @package Pl\CommonBundle\Manager
 * @property string $nodePath
 * @property string $lessModulesPath
 * @property KernelInterface $kernel
 */
class NodeManager{
	protected $nodePath;
	protected $lessModulesPath;
	protected $kernel;

	/**
	 * AssetManager constructor.
	 * @param string $nodePath
	 * @param string $lessModulesPath
	 * @param KernelInterface $kernel
	 */
	public function __construct(KernelInterface $kernel, $nodePath, $lessModulesPath){
		$this->nodePath = $nodePath;
		$this->lessModulesPath = $lessModulesPath;
		$this->kernel = $kernel;

	}

	/**
	 * @param $lessFilePath
	 * @return string
	 */
	public function compileFileWithLess($fromLessPath, $destCssPath, $replaceCallback){
		$command = sprintf("%s %s/less/bin/lessc $fromLessPath"
			, $this->nodePath
			, $this->lessModulesPath
		);

		try{
			$cssString = $this->doCommand($command);
		}
		catch(\Exception $e){
			throw new GenerateCssException($e->getMessage());
		}



		$cssString = $replaceCallback($cssString);
		file_put_contents($destCssPath, $cssString);
	}

	/**
	 * @param $cssFilePath
	 */
	public function uglifyFile($cssFilePath){
		$command = sprintf("%s %s/uglifycss/uglifycss $cssFilePath"
			, $this->nodePath
			, $this->lessModulesPath
			, $this->lessModulesPath
		);
		$uglifiedString = $this->doCommand($command);
		file_put_contents($cssFilePath, $uglifiedString);
	}


	/**
	 * @param $command
	 * @return false|string|null
	 * @throws \Exception
	 */
	public function doCommand(string $command){
		$errorPath = sprintf("%s/error-output.txt", $this->kernel->getCacheDir());

		$descriptorspec = [
			0 => ["pipe", "r"],  // // stdin est un pipe où le processus va lire
			1 => ["pipe", "w"],  // // stdin est un pipe où le processus va lire
			2 => ["file", $errorPath, "w"]
		];
		$process = proc_open($command, $descriptorspec, $pipes);
		if(is_resource($process)) {
			$return = stream_get_contents($pipes[1]);
			$err = file_get_contents($errorPath);
			if(strlen($err)){
				throw new \Exception($err);
			}
			fclose($pipes[1]);
			proc_close($process);
			unlink($errorPath);
			return $return;
		}
		unlink($errorPath);
		return null;
	}
}
