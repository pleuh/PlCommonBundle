<?php

namespace Pl\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FilePickerController extends Controller
{
    public function download(Request $request)
    {


    	$fileInfos = $request->request->get("file");
    	$path = $request->request->get("path");
    	$m = array();
    	if(preg_match('#.+\.(.+)$#', $fileInfos["filename"], $m)){
    		$extension = $m[1];
    		$url = $fileInfos["url"];

       	 	$destinationFile = $path.uniqid('', true).'.' . $extension;
			// $completeUrl = $this->get('request')->getBasePath().'/'.$destinationFile;
			$content = file_get_contents($url);
			file_put_contents($destinationFile, $content);

			//Delete former file
			$oldFile = $request->request->get("oldFile");
	    	if($oldFile != ""){
	    		$m = array();
	    		if(preg_match('#'.$request->getBasePath().'/(.+)$#', $oldFile, $m) && file_exists($m[1])){
	    			unlink($m[1]);
	    		}
	    	}

			return new Response(json_encode($destinationFile));
    	}
    	throw new \Exception("Impossible to get the extension of the filepick file");
    }
}
