<?php

namespace Pl\CommonBundle\Controller;

use Pl\CommonBundle\Interfaces\SitemapProviderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\Response;


class SitemapController extends Controller{


	public function sitemap(){
		$urls = [];
		/** @var SitemapProviderInterface $provider */
		try{
			$parameter = $this->getParameter("pl_common.sitemap_provider");
			$provider = $this->get($parameter);
			$urls = $provider->getAllUrls();
		}
		catch(ServiceNotFoundException $e){
			throw new \RuntimeException($e);
		}
		catch(InvalidArgumentException $e){
			//parameter does not exists
			//silent error
		}
		$content = $this->renderView('PlCommonBundle:Sitemap:sitemap.xml.twig', [
			'urls' => $urls,
		]);
		$textResponse = new Response($content , 200);
		$textResponse->headers->set('Content-Type', 'text/xml');
		return $textResponse;
	}


	public function robots(){
		$content = $this->renderView('PlCommonBundle:Sitemap:robots.txt.twig', [
			"url" => $this->get("router")->generate("pl_sitemap"),
			"prevent_indexation" => $this->getParameter("pl_common.prevent_indexation"),
		]);
		$textResponse = new Response($content , 200);
		$textResponse->headers->set('Content-Type', 'text/plain');
		return $textResponse;
	}

}




