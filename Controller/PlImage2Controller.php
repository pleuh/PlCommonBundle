<?php

namespace Pl\CommonBundle\Controller;

use Imagine\Image\ImagineInterface;
use Pl\CommonBundle\Manager\ImageManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class PlImage2Controller extends Controller
{

	private function cleanTmpFiles(){
		//Clean files
		foreach((new Finder())->in($this->container->getParameter("pl_common.upload_tmp_dir")) as $file){
			if(time() - $file->getCTime() > 60){
				unlink($file);
			}
		}
	}

	public function download(Request $request, Packages $assets){
		try{
			if($request->files->has('file')){

				$imageManager = $this->get(ImageManager::class);
				$file = $request->files->get('file');
				$ext = preg_replace("#jpeg#", "jpg", $file->guessExtension());
				if(!preg_match('#(jpg|png|bmp|tif|svg|gif)#', strtolower($ext))){
					throw new \Exception("bad file format");
				}

				$uploadDir = $this->container->getParameter("pl_common.upload_tmp_dir");
				if(!file_exists($uploadDir)){
					mkdir($uploadDir, 0777, true);
				}
				$this->cleanTmpFiles();

				do{
					$filename = base64_encode(uniqid()) . "." . $ext;
				} while(file_exists($uploadDir . $filename));


				$file->move($uploadDir, $filename);
				$fullPath = $uploadDir . $filename;

				if(strtolower($ext) != "svg" && strtolower($ext) != "gif"){
					$imageManager->redimensionerImage($fullPath, $request->get("width"), $request->get("height"), $request->get("toFormat"));
				}
				if($request->get("toFormat")){
					$fullPath = $imageManager->convertToFormat($fullPath, $request->get("toFormat"));
				}

				return new Response(json_encode([
					"assetpath" => $assets->getUrl($fullPath),
					"filepath" => $fullPath,
				]));
			}
		}
		catch(\Exception $e){
			return new Response(json_encode(["error" => $e->getMessage()]));
		}

		return new Response(json_encode("No file received"));
	}
}
