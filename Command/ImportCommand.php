<?php

namespace Pl\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class ImportCommand extends ContainerAwareCommand
{

	protected function getString($string){
		if($string != "" && $string != "ND"){
			return $string;
		}
		return null;
	}
	protected function getDatetime($string, $format = "d/m/Y"){
		return date_create_from_format($format, $string);
	}
	protected function getBoolean($string){
		if(strtolower($string) == "oui"){
			return true;
		}
		return false;
	}
	protected function getInteger($string){
		return (int)$string;
	}
	protected function getTelephone($string){
		if(preg_match('#(\d{2})[ .-]*(\d{2})[ .-]*(\d{2})[ .-]*(\d{2})[ .-]*(\d{2})[ .-]*#', $string, $m)){
			return sprintf("%s%s%s%s%s", $m[1], $m[2], $m[3], $m[4], $m[5]);
		}
		return null;
	}
}
