<?php
namespace Pl\CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Siret  extends Constraint
{
    public $message = 'Ce numéro de SIRET semble invalide';

    
}