<?php
namespace Pl\CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SiretValidator extends ConstraintValidator{

	public function validate($value, Constraint $constraint){
		if($value != null && !preg_match('#^\w+[\s]*[0-9]{3}[\s]*[0-9]{3}[\s]*[0-9]{3}$#', $value, $matches)){
			$this->context->addViolation($constraint->message);
		}
	}
}

