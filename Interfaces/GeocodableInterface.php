<?php

namespace Pl\CommonBundle\Interfaces;


interface GeocodableInterface
{
	public function getLat();
	public function getLng();
	public function setLat($lat);
	public function setLng($lng);
	public function getHasBeenGeocoded();
	public function setHasBeenGeocoded($hasBeenGeocoded);
}
