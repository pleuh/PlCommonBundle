<?php

namespace Pl\CommonBundle\Interfaces;



interface SitemapProviderInterface{
	/**
	 * @return SitemapUrl[]
	 */
	public function getAllUrls();
}



class SitemapUrl{
	protected $loc;
	protected $lastmod;
	protected $changefreq;
	protected $priority;

	/**
	 * SitemapUrl constructor.
	 * @param $loc
	 */
	public function __construct($loc, $priority = null, $lastmod = null, $changefreq = null){
		$this->loc = $loc;
		$this->priority = $priority;
		$this->lastmod = $lastmod;
		$this->changefreq = $changefreq;
	}


	/**
	 * @return mixed
	 */
	public function getLoc(){
		return $this->loc;
	}


	/**
	 * @return mixed
	 */
	public function getLastmod(){
		return $this->lastmod;
	}

	/**
	 * @param mixed $lastmod
	 */
	public function setLastmod($lastmod){
		$this->lastmod = $lastmod;
	}

	/**
	 * @return mixed
	 */
	public function getChangefreq(){
		return $this->changefreq;
	}

	/**
	 * @param mixed $changefreq
	 */
	public function setChangefreq($changefreq){
		$this->changefreq = $changefreq;
	}

	/**
	 * @return mixed
	 */
	public function getPriority(){
		return $this->priority;
	}

	/**
	 * @param mixed $priority
	 */
	public function setPriority($priority){
		$this->priority = $priority;
	}
}