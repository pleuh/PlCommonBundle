<?php
 
namespace Pl\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
 
class DateTimePickerType extends AbstractType
{ /**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'       =>  null,
            'attr' => [
                'autocomplete' => 'off',
                'class' => 'pl_datetimepicker',
			],
		]);
    }

	/**
	 * @param array $options
	 * @return array
	 */
	public function getDefaultOptions(array $options)
    {
        return array(
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy HH:mm',
            'attr' => array(
                'autocomplete' => 'off',
                'class' => 'pl_datetimepicker',
            ),
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new \Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer(null, null, 'd/m/Y H:i');
        $builder->addModelTransformer($transformer);
    }

 

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class'       =>  null,
            'attr' => [
                'autocomplete' => 'off',
                'class' => 'pl_datetimepicker',
			],
		]);

    }
 
    public function getName(){
        return 'pl_datetimepicker';
    }

    public function getParent(){
        return TextType::class;
    }
}