<?php
 
namespace Pl\CommonBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
 
class ImagePickerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'type'       =>  "image",
            'services'       =>  array('COMPUTER', 'URL', 'FACEBOOK', 'GMAIL', 'DROPBOX', 'INSTAGRAM'),
            'width'       =>  null,
            'height'       =>  null,
            'defaultImage'       =>  null,
            'displayButton'       =>  true,
            'fit'       =>  'crop',
            'path'       =>  'uploads/others/',
        ));
    }
  /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'type'       =>  "image",
            'services'       =>  array('COMPUTER', 'URL', 'FACEBOOK', 'GMAIL', 'DROPBOX', 'INSTAGRAM'),
            'width'       =>  null,
            'height'       =>  null,
            'defaultImage'       =>  null,
            'displayButton'       =>  true,
            'fit'       =>  'crop',
            'path'       =>  'uploads/others/',
        ));

    }

  /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if($options["type"] == "image" && ($options['width'] == null && $options['height'] == null)){
          throw new \Exception("You have to define a width and a height for a filepicker image type");
        }
        $view->vars["services"] = $options['services'];
        $view->vars["type"] = $options['type'];
        $view->vars["width"] = $options['width'];
        $view->vars["height"] = $options['height'];
        $view->vars["defaultImage"] = $options['defaultImage'];
        $view->vars["displayButton"] = $options['displayButton'];
        $view->vars["path"] = $options['path'];
        $view->vars["fit"] = $options['fit'];
    }

    
    public function getParent()
    {
        return 'text';
    }
 
    public function getName()
    {
        return 'pl_imagepicker';
    }
}