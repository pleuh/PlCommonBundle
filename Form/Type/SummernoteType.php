<?php

namespace Pl\CommonBundle\Form\Type;

use Pl\CommonBundle\Form\DataTransformer\SummernoteTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SummernoteType extends \Symfony\Component\Form\Extension\Core\Type\TextareaType
{

	public function __construct(){
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildView(FormView $view, FormInterface $form, array $options){
		$view->vars["placeholder"] = $options['placeholder'];
	}


	public function buildForm(FormBuilderInterface $builder, array $options){
		$builder->addModelTransformer(new SummernoteTransformer($options["allowedTags"]));
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver){
		$resolver->setDefaults([
			'allowedTags' => [],
			'compound' => false,
			'placeholder' => null
		]);
	}



	public function getName(){
		return 'pl_summernote';
	}

	public function getBlockPrefix(){
		return 'pl_summernote';
	}

}