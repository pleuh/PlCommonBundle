<?php

namespace Pl\CommonBundle\Form\Type;
use Pl\CommonBundle\Form\DataTransformer\RoleAdminArrayTransformer;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;


class RoleAdminArrayType extends ImmutableArrayType
{

    private $transformer;

    public function __construct(RoleAdminArrayTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer($this->transformer);
    }

    public function getName()
    {
        return 'pl_roleadminarraytype';
    }

    public function getParent()
    {
        return 'sonata_type_immutable_array';
    }
}