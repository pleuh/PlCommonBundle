<?php
 
namespace Pl\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
 
class PlImage2Type extends \Symfony\Component\Form\Extension\Core\Type\TextType
{

  /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'width' =>  null,
            'height' =>  null,
            'empty_template' =>  null,
            'data_class' => null,
			'to_format' => null,
            'compound' => false,
            'required' => false
        ]);
    }

  /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'width' =>  null,
            'height' =>  null,
            'empty_template' =>  null,
            'data_class' => null,
            'to_format' => null,
            'required' => false
        ]);
    }


	public function buildForm(FormBuilderInterface  $builder, array $options)
	{
		parent::buildForm($builder, $options);
	}


	/**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if($options['width'] == null && $options['height'] == null){
          throw new \Exception("You have to define a width and a height for a plimage2 type");
        }
        $view->vars["width"] = $options['width'];
        $view->vars["height"] = $options['height'];
        $view->vars["empty_template"] = $options['empty_template'];
        $view->vars["to_format"] = $options['to_format'];
    }


    public function getBlockPrefix()
    {
        return 'pl_image2';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}