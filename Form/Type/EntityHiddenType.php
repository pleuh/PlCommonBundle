<?php
 
namespace Pl\CommonBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Pl\CommonBundle\Form\DataTransformer\EntityToIdTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager;
 
class EntityHiddenType extends HiddenType
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
 
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
 
    public function buildForm(FormBuilderInterface $builder, array $options){
        $transformer = new EntityToIdTransformer($this->objectManager, $options['class']);
        $builder->addModelTransformer($transformer);
    }
 
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired(['class'])
            ->setDefaults([
                'invalid_message' => 'The entity does not exist.',
				'compound' => false,
			])
        ;
    }

    public function getName()
    {
        return 'pl_entity_hidden';
    }
}