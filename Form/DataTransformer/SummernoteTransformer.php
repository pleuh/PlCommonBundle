<?php
namespace Pl\CommonBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class SummernoteTransformer implements DataTransformerInterface
{
	protected $allowedTags;
	public const ALLOW_EVERYTHING = "everything";

	public function __construct($allowedTags){
		if($allowedTags == self::ALLOW_EVERYTHING){
			$this->allowedTags = self::ALLOW_EVERYTHING;
			return;
		}
		$defaultAllowedTags = [
			"u",
			"br",
			"h1",
			"h2",
			"h3",
			"h4",
			"h5",
			"h6",
			"h7",
			"em",
			"table",
			"thead",
			"tbody",
			"td",
			"th",
			"tr",
			"img",
			"a",
			"b",
			"blockquote",
			"del",
			"dd",
			"dl",
			"dt",
			"em",
			"i",
			"img",
			"li",
			"ol",
			"p",
			"pre",
			"s",
			"sup",
			"sub",
			"strong",
			"strike",
			"ul",
			"br",
			"span",
			"iframe",
			"hr",
		];
		$this->allowedTags = array_merge($defaultAllowedTags, $allowedTags);
	}

	private function sanitizeTexte($inputStr){
		if($this->allowedTags == self::ALLOW_EVERYTHING){
			return $inputStr;
		}

		$strTags = "";
		foreach($this->allowedTags as $allowedTag){
			$strTags .= sprintf("<%s>", $allowedTag);
		}

		return strip_tags($inputStr, $strTags);
	}

	/**
	 *
	 * @param  string $inputStr
	 * @return string
	 */
	public function transform($inputStr){
		if(null === $inputStr){
			return "";
		}
	
		return $inputStr;
	}

	/**
	 *
	 * @param  string $inputStr
	 * @return string
	 */
	public function reverseTransform($inputStr){
		if(!$inputStr){
			return null;
		}

		return $this->sanitizeTexte($inputStr);
	}
}