<?php


namespace Pl\CommonBundle\Form\DataTransformer;

use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;


class CamelCaseTransformer implements DataTransformerInterface
{

	public function transform($value){
		return $value;
	}


	public function reverseTransform($value){

		$ret = (new CamelCaseToSnakeCaseNameConverter())->denormalize(Urlizer::urlize((new CamelCaseToSnakeCaseNameConverter())->normalize($value), "_"));
		return $ret;
	}
}
