<?php

namespace Pl\CommonBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class RoleAdminArrayTransformer implements DataTransformerInterface
{

    public function __construct()
    {
        // You'll need the $manager to lookup your objects later
    }

    public function reverseTransform($value)
    {
        if (is_null($value)) {
            return $value;
        }

        $arRet = array();
        foreach ($value as $key => $val) {
            if($val){
                array_push($arRet, $key);
            }
        }
        return $arRet;
    }

    public function transform($value)
    {
        if (is_null($value)) {
            return $value;
        }

        // Here convert ids embedded in your array to objects, 
        // or ArrayCollection containing objects

        $arRet = array();
        foreach ($value as $key => $val) {
            $arRet[$val] = true;
        }
        dump($value);
        dump($arRet);
        return $arRet;
    }

}