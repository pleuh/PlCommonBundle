<?php
namespace Pl\CommonBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class RemoveSpacesTransformer implements DataTransformerInterface
{

    /**
     * Transforms an object (Promotion) to a string (code).
     *
     * @param string $promotion
     * @return string
     */
    public function transform($value)
    {

        return $value;
    }

    /**
     * Transforms a string (code) to an object (Promotion).
     *
     * @param  string $value
     * @return string
     * @throws TransformationFailedException if object (Promotion) is not found.
     */
    public function reverseTransform($value)
    {

        $value = preg_replace('#[ ]#', '', $value);
        return $value;

    }
}