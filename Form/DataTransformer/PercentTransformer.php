<?php


namespace Pl\CommonBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;


class PercentTransformer implements DataTransformerInterface
{

	public function transform($value){
		if(null === $value || $value == ''){
			return '';
		}

		return sprintf("%s%%", $value * 100);
	}


	public function reverseTransform($value){
		if(empty($value)){
			return;
		}

		$value = (float)preg_replace('#,#', ".", $value) / 100;

		return $value;
	}
}
