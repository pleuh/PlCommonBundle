<?php
namespace Pl\CommonBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class TelephoneTransformer implements DataTransformerInterface
{

    private $limitPortables;
    private $limitLength;
    public function __construct($limitPortables = false, $limitLength = false)
    {
        $this->limitPortables = $limitPortables;
        $this->limitLength = $limitLength;
    }

    private function sanitizePhone($phoneStr){
        $phoneStr = preg_replace("#[ )(+\.]#", "", $phoneStr);

        if(strlen($phoneStr) == 12 and preg_match('#^(\d\d)0#', $phoneStr, $m)){
            $phoneStr = $m[1] . substr($phoneStr, 3);
        }
        if(strlen($phoneStr) == 10 and preg_match('#^0#', $phoneStr)){
            $phoneStr = "33" . substr($phoneStr, 1);
        }

        $phoneStr = "+".$phoneStr;
        return $phoneStr;
    }

    private function isCorrect($phoneStr){
    	if(!$this->limitLength){
			return preg_match('#\+\d+$#', $phoneStr);
		}
		else{
			return preg_match('#\+\d{11,12}$#', $phoneStr);
		}
    }

    private function testPhone(){
        $arPhones = [
                '+12 6 46 97 32 03',
                '+33 6 78 57 40 25',
                '0592700351',
                '02 16 49 53 17',
                '+33 1 94 46 94 41',
                '05',
                '09 37 81 53 27',
                '0293114746',
                '0336729285',
                '05 91 37 64 61',
                '+33 9 80 59 78 13',
                '0188295717',
                '04 27 42 79 99',
                '+33 2 92 85 27 37',
                '04 15 19 77 03',
                '+33 2 24 19 75 15',
                '0199340349',
                '+33 1 29 24 76 20',
                '02 91 62 85 61',
                '+33 4 69 68 04 83',
                '0192510800',
                '+33 (0)9 12 55 68 20',
                '06 79 10 55 56',
                '0307329781',
                '03 96 98 51 46',
                '+33 (0)8 73 50 48 85',
                '0748369324',
                '+33 2 53 69 59 76',
                '+33 (0)3 03 81 89 49',
                '0932613183',
                '+39 (0)1 75 27 01 75',
                '06 18 71 29 96',
                '+33 (0)3 25 96 72 12',
                '+33 9 64 98 24 51',
                '0231761714',
                '0165482183',
                '+33 7 58 99 07 13',
                '0615460716',
                '+33 5 30 96 74 61',
                '0126502670',
                '+33 4 31 68 85 70',
                '+33 (0)1 36 27 71 33',
                '05 73 91 75 03',
                '+33 (0)2 74 84 73 78',
                '+33 (0)4 40 49 74 53',
                '0660176381',
                '+33 1 32 55 97 60',
                '01 43 73 63 44',
                '01 78 87 09 90',
                '+33 (0)1 47 81 05 93',
                '05 52 21 97 64',
                '0108331658',
                '01 33 75 08 43',
                '+33 (0)7 78 52 27 08',
                '+33 1 68 87 92 67',
                '+33 4 83 46 81 39',
                '09 35 27 16 97',
                '0174312664',
                '+33 1 66 15 68 82',
                '0656900783',
                '+33 1 78 71 05 85',
                '+33 6 21 58 20 64',
                '05 94 57 58 47',
                '0581592071',
                '04 06 81 11 12',
                '+33 (0)5 27 76 18 25',
                '04 08 37 06 37',
                '+33 3 21 54 84 87',
                '+33 9 43 20 06 39',
                '0193129816',
                '+33 (0)1 81 77 04 89',
                '+33 2 74 06 25 16',
                '+33 1 48 69 55 45',
                '0957765492',
                '+33 3 28 60 38 87',
                '0849984854',
                '0978774477',
                '+33 4 81 14 23 01',
                '0412815615',
                '+33 4 31 78 12 03',
                '+33 9 99 58 04 38',
                '+33 (0)4 10 75 09 11',
                '04 93 95 40 24',
                '+33 (0)1 54 33 43 02',
                '0721618034',
                '04 56 74 31 60',
                '+33 (0)9 46 35 15 18',
                '+33 (0)1 25 22 80 57',
                '+33 (0)1 00 66 59 10',
                '+33 4 99 69 12 13',
                '0247156789',
                '03 39 31 72 24',
                '+33 4 72 53 26 10',
                '+33 (0)3 75 87 23 66',
                '02 37 87 21 35',
                '01 74 17 76 22',
                '+33 (0)3 38 89 80 10',
                '05 35 73 02 56',
                '0944972937',
                '+33 8 09 59 40 60',
                'telephone',
                ' ',
                '01 15 59 68 87',
                '0564832950',
                '0109123277',
                '+33 2 77 36 83 50',
                '0664514293',
                '04 24 55 20 55',
                '+33 (0)9 54 67 51 21',
                '0818965155',
                '0709274579',
                '0377552691',
                '+33 (0)1 51 05 60 71',
                '0501644023',
                '04 27 82 64 64',
                '0782228255',
                '+33 1 96 92 05 09',
                '05 54 01 22 63',
                '01 16 36 07 46',
                '+33 (0)2 36 46 22 27',
                '0673455238',
                '04 29 34 60 88',
                '+33 (0)8 40 31 96 14',
                '09 22 05 39 13',
                '+33 1 54 19 15 56',
                '+33 4 09 26 80 09',
                '+33 7 26 67 49 14',
                '0889550392',
                '04 25 64 71 64',
                '+33 (0)1 25 21 25 52',
                '0997472741',
                '+33 5 78 86 73 14',
                '+33 (0)3 66 27 01 49',
                '+33 7 71 43 81 63',
                '06 60 98 13 47',
                '+33 (0)4 00 54 57 27',
                '+33 8 50 04 31 74',
		];

        foreach ($arPhones as $phone) {
            printf('%s => %s | %s<br/>', $phone, $this->sanitizePhone($phone), $this->isCorrect($this->sanitizePhone($phone)) ? "CORRECT" : "INCORRECT");
        }
    }

    /**
     * Transforms an object (Promotion) to a string (code).
     *
     * @param  Promotion|null $promotion
     * @return string
     */
    public function transform($phoneStr)
    {
        if (null === $phoneStr) {
            return "";
        }
        if(preg_match('#^\+(\d{3})(\d{2})(\d{2})(\d{2})(\d{2})$#', $phoneStr, $m)){
            return sprintf('+%s %s %s %s %s', $m[1], $m[2], $m[3], $m[4], $m[5]);
        }
        return $phoneStr;
    }

    /**
     * Transforms a string (code) to an object (Promotion).
     *
     * @param  string $code
     * @return Promotion|null
     * @throws TransformationFailedException if object (Promotion) is not found.
     */
    public function reverseTransform($phoneStr)
    {
        if (!$phoneStr) {
            return null;
        }

        $returnString = $this->sanitizePhone($phoneStr);

        if(!$this->isCorrect($returnString)){
            throw new TransformationFailedException("Impossible de comprendre le format de votre numéro de téléphone");
        }
        if($this->limitPortables != null){
            if(!preg_match('#^\+33(6|7)#', $returnString)){
                return "mauvais indicatif";
            }
        }
        return $returnString;
    }
}
